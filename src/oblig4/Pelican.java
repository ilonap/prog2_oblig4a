package oblig4;

public class Pelican extends Oviparous implements Flyable, Walkable {


    public Pelican(String name, int code) {
        super(name, code);
    }

    @Override
    public boolean fly() {
        System.out.print("flies...");
        return true;
    }

    @Override
    public boolean walk() {
        return true;
    }
}
