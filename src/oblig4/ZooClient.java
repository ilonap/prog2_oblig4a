package oblig4;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;


public class ZooClient {
    public static void main(String[] args) {

        Zoo zoo = new Zoo("Kristiansand Dyrepark");

        Collection<Animal> animals = new ArrayList<>();

        animals.add(new Crocodile("Crocodylus niloticus", 1001));
        animals.add(new Crocodile("Crocodylus niloticus", 1002));
        animals.add(new Crocodile("Crocodylus porosus", 1101));
        animals.add(new Crocodile("Crocodylus porosus", 1102));

        animals.add(new Pelican("Brown Pelican  ", 4001));
        animals.add(new Pelican("Dalmatian Pelican  ", 4101));

        animals.add(new Whale("Blue whale", 2001));
        animals.add(new Whale("Blue whale", 2002));
        animals.add(new Whale("Minke whale", 2101));
        animals.add(new Whale("Minke whale", 2102));

        animals.add(new Bat("Acerodon ", 3001));
        animals.add(new Bat("Cistugo  ", 3002));

        zoo.setAnimals(animals);

        zoo.getAnimals().stream()
                .filter(e -> e instanceof Flyable)
                .forEach(e -> {
                    System.out.print(e. getName());
                    ((Flyable) e).fly();
                    System.out.println();
                });

        zoo.getAnimals().stream()
                .filter(e -> e instanceof Mammal && e instanceof Jumpable)
                .forEach(e -> {
                    System.out.print(e.getName() + " ");
                    ((Jumpable) e).jump();
                    System.out.println();
                });

        List<Animal> walker = zoo.getAnimals().stream().filter(p -> p instanceof Walkable).collect(Collectors.toList());

        // deloppgave a
//        try {
//            walker.stream().forEach(p -> ((Flyable) p).fly());
//        } catch (Exception e) {
//            System.out.println("Cold not apply method: " + e.getMessage());
//            e.printStackTrace();
//        }


        // deloppgave b
        // todo skjønner ikke helt hva dem mener ved "Programmet skal stoppe helt"
//        try {
//            walker.stream().forEach(p -> ((Flyable) p).fly());
//        } catch (Exception e) {
//            return; // enten eller
//            //System.exit(0);
//        }
//        System.out.println("This is not going to be displayed");


        // deloppgave c
        walker.forEach(p -> {
            try {
                System.out.print(p.getName() + " ");
                p.fly();
                System.out.println();
            } catch (ZooException e) {
                System.out.println(e.getMessage());
            }
        });
    }
}