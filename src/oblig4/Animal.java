package oblig4;

public abstract class Animal {
    public final String name;
    public final int animalID;

    public Animal(String name, int code) {
        this.name = name;
        this.animalID = code;
    }
    @Override
    public String toString() {
        return "Animal [name=" + name + ", code=" + animalID + "]";
    }

    public String getName() {
        return name;
    }

    public int getAnimalID() {
        return animalID;
    }

    public boolean fly() throws ZooException {
        throw new ZooException(this + " can not fly!");
    }

    public boolean jump() throws ZooException {
        throw new ZooException(this + " can not jump!");
    }

    public boolean walk() throws ZooException {
        throw new ZooException(this + " can not walk!");
    }

    public boolean swim() throws ZooException {
        throw new ZooException(this + " can not swim!");
    }
}
